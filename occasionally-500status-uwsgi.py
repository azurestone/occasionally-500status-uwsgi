import datetime
import random

def application(env, start_response):

    date = datetime.datetime.now()
    rand = int(random.uniform(1,10000))

    if date.second == 5 and rand == 10:
        start_response('500 Internal Server Error', [('Content-Type','text/html')])
        return [b"<!DOCTYPE html><html><head><title>sample / 200</title></head><body><p>Hello World!</p></body></html>"]

    else:
        start_response('200 OK', [('Content-Type','text/html')])
        return [b"<!DOCTYPE html><html><head><title>sample / 500</title></head><body><p>Hello World!</p></body></html>"]

# ref:
#  https://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html

