# README #

** 注意：まったく業務で活用できないスクリプト **

HTTPクライアントが、HTTPサーバーにアクセスした時に5秒および乱数によって10だった場合に 500 Internal Server Error と返答する。

## Usage

```
uwsgi --http :4649 --wsgi-file occasionally-500status-uwsgi.py
```

## setup

```
cat requirements.txt | xargs -I{} pip3 install {}
```

